class CreatePerscriptionPositions < ActiveRecord::Migration
  def change
    create_table :prescription_positions do |t|
      t.string :medicine, null: false
      t.string :dose, null: false
      t.string :usage
      t.string :type
      t.references :prescription, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
