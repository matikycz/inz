class AddNurseAndPatientToExamination < ActiveRecord::Migration
  def change
  	add_reference :examinations, :nurse, references: :users, index: true
  	add_reference :examinations, :patient, references: :users, index: true
  end
end
