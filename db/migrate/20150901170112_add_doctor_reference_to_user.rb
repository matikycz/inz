class AddDoctorReferenceToUser < ActiveRecord::Migration
  def change
  	add_reference :users, :doctor, index: true, foreign_key: true
  end
end
