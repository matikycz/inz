class CreateAlergicMedicines < ActiveRecord::Migration
  def change
    create_table :alergic_medicines do |t|
      t.string :name, null: false
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
