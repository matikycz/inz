class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :degree,             null: false, default: ""
      t.string :specialization,     null: false, default: ""
      t.string :doctorRightNumber,  null: false, limit: 7
      t.date :employmentDate,       null: false
      t.date :dismissalDate,        null: true

      t.timestamps null: false
    end
  end
end
