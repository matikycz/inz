class ChangeUserColumnOrder < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.change :sex, :integer, after: :surname, null: false, default: 0
      t.change :login, :string, after: :sex, null: false, default: "", uniq: true
      t.change :encrypted_password, :string, after: :login, null: false, default: ""
      t.change :email, :string, after: :encrypted_password, null: false, default: "", uniq: true
      t.change :city_id, :integer, after: :email, index: true, foreign_key: true, null: false
      t.change :street, :string, after: :city_id, null: false, default: ""
      t.change :address, :string, after: :street, null: false, default: ""
      t.change :pesel, :string, after: :address, null: false, default: "", limit: 11, uniq: true
      t.change :phone, :string, after: :pesel, null: false, default: "", limit: 12
    end
  end
end
