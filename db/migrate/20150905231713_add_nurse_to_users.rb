class AddNurseToUsers < ActiveRecord::Migration
  def change
  	add_reference :users, :nurse, index: true, foreign_key: true
  end
end
