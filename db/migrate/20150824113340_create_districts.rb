class CreateDistricts < ActiveRecord::Migration
  def change
    create_table :districts do |t|
      t.string :name, null: false
      t.references :province, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
