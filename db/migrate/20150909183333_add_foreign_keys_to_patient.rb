class AddForeignKeysToPatient < ActiveRecord::Migration
  def change
  	add_foreign_key :patients, :nfz_branches, column: :nfzbranch_id
  	add_foreign_key :patients, :doctors, column: :maindoctor_id
  end
end
