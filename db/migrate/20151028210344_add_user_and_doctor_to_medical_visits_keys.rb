class AddUserAndDoctorToMedicalVisitsKeys < ActiveRecord::Migration
  def change
  	add_foreign_key :medical_visits, :users, column: :patient_id
  	add_foreign_key :medical_visits, :users, column: :doctor_id
  end
end
