class AddDeseaseToVisitDescription < ActiveRecord::Migration
  def change
  	add_reference :visit_descriptions, :disease, index: true, foreign_key: true
  end
end
