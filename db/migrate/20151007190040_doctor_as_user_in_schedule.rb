class DoctorAsUserInSchedule < ActiveRecord::Migration
  def change
  	add_reference :schedules, :doctor, references: :users
  end
end
