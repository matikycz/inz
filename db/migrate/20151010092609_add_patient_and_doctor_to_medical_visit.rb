class AddPatientAndDoctorToMedicalVisit < ActiveRecord::Migration
  def change
  	add_reference :medical_visits, :doctor, references: :users
  	add_reference :medical_visits, :patient, references: :users
  end
end
