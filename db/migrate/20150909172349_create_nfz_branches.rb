class CreateNfzBranches < ActiveRecord::Migration
  def change
    create_table :nfz_branches do |t|
      t.string :identifier, null: false, length: 2
      t.string :name, 			null: false

      t.timestamps null: false
    end
  end
end
