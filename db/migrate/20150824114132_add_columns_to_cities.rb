class AddColumnsToCities < ActiveRecord::Migration
  def change
    add_column :cities, :commune, :string, null: false
    add_reference :cities, :province, index: true, foreign_key: true, null: false
    add_reference :cities, :district, index: true, foreign_key: true, null: false
  end
end
