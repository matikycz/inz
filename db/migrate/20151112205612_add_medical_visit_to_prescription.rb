class AddMedicalVisitToPrescription < ActiveRecord::Migration
  def change
  	add_reference :prescriptions, :medical_visit, index: true, foreign_key: true
  end
end
