class AddIndicesOnCity < ActiveRecord::Migration
  def change
  	add_index :cities, :name
  	add_index :cities, :city_code
  end
end
