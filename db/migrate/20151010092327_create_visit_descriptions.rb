class CreateVisitDescriptions < ActiveRecord::Migration
  def change
    create_table :visit_descriptions do |t|
      t.string :symptoms
      t.string :examDescription
      t.string :diagnosis
      t.string :treatment

      t.timestamps null: false
    end
  end
end
