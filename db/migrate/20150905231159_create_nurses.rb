class CreateNurses < ActiveRecord::Migration
  def change
    create_table :nurses do |t|
      t.string :degree,             null: false, default: ""
      t.string :specialization,     null: false, default: ""
      t.string :nurseRightNumber,  null: false, limit: 7
      t.date :employmentDate,       null: false
      t.date :dismissalDate,        null: true

      t.timestamps null: false
    end
  end
end
