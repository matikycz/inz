class CreateMedicalVisits < ActiveRecord::Migration
  def change
    create_table :medical_visits do |t|
      t.datetime :startTime, null: false
      t.datetime :endTime, null: false
      t.boolean :ended, null: false, default: false
      t.references :visit_description, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
