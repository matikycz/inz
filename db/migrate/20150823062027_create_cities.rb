class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name,				null: false, default: ""
      t.string :city_code,	null: false, default: "", limit: 6

      t.timestamps null: false
    end
  end
end
