class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.integer :number, null: false, uniq: true
      t.datetime :drawDate, null: false
      t.datetime :realizationDate, null: false
      t.boolean :isConfirmed, null:false, default: false

      t.timestamps null: false
    end
  end
end
