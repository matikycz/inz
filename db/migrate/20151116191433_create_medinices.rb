class CreateMedinices < ActiveRecord::Migration
  def change
    create_table :medicines do |t|
      t.string :name
      t.string :composition
      t.string :form
      t.string :category
      t.string :specialization
      t.string :effect
      t.string :advice, :limit => 10000
      t.string :dose, :limit => 10000

      t.timestamps null: false
    end
  end
end
