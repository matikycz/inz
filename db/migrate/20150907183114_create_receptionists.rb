class CreateReceptionists < ActiveRecord::Migration
  def change
    create_table :receptionists do |t|
      t.string :position,             null: false, default: ""
      t.date :employmentDate,       null: false
      t.date :dismissalDate,        null: true

      t.timestamps null: false
    end
  end
end
