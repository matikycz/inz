class CreateExaminations < ActiveRecord::Migration
  def change
    create_table :examinations do |t|
      t.string :name, null: false
      t.datetime :date, null: false
      t.string :description, null: false
      t.boolean :isconfirmed, null: false, default: false

      t.timestamps null: false
    end
  end
end
