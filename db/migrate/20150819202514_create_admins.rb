class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.boolean :ismain, null: false

      t.timestamps null: false
    end
  end
end
 