class AddRecetpionistToUser < ActiveRecord::Migration
  def change
  	add_reference :users, :receptionist, index: true, foreign_key: true
  end
end
