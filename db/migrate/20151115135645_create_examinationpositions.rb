class CreateExaminationpositions < ActiveRecord::Migration
  def change
    create_table :examinationpositions do |t|
      t.string :name, null: false
      t.string :result, null: false
      t.string :norm, null: false
      t.references :examination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
