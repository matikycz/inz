class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.datetime :startTime, null: false
      t.datetime :endTime, null: false
      t.integer :interval, null: false
      t.datetime :intervalEnd, null: true
      t.integer :doctor, null: false
      t.references :room, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
