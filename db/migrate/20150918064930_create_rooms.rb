class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :number, null: false, default: "", unique: true
      t.string :name, null: false, default: ""

      t.timestamps null: false
    end
  end
end
