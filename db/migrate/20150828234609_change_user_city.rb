class ChangeUserCity < ActiveRecord::Migration
  def change
    remove_column :users, :city_code
    add_reference :users, :city, index: true, foreign_key: true, null: false
  end
end
