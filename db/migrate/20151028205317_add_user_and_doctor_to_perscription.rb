class AddUserAndDoctorToPerscription < ActiveRecord::Migration
  def change
  	add_reference :prescriptions, :doctor, references: :users
  	add_reference :prescriptions, :patient, references: :users
  end
end
