class UpdateStreetField < ActiveRecord::Migration
  def change
  	change_column :users, :street, :string, null: true
  end
end
