class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :insurancenumber, null: false, length: 13
      t.date :registrationdate, null: false
      t.references :nfzbranch, references: :nfz_branches, index: true
      t.references :maindoctor, references: :doctors, index: true
      t.timestamps null: false
    end
  end
end
