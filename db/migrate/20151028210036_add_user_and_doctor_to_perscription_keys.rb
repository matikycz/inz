class AddUserAndDoctorToPerscriptionKeys < ActiveRecord::Migration
  def change
  	add_foreign_key :prescriptions, :users, column: :patient_id
  	add_foreign_key :prescriptions, :users, column: :doctor_id
  end
end
