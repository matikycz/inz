# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151116191433) do

  create_table "admins", force: :cascade do |t|
    t.boolean  "ismain",     limit: 1, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "alergic_medicines", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "alergic_medicines", ["user_id"], name: "index_alergic_medicines_on_user_id", using: :btree

  create_table "chronic_diseases", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "chronic_diseases", ["user_id"], name: "index_chronic_diseases_on_user_id", using: :btree

  create_table "chronic_medicines", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "chronic_medicines", ["user_id"], name: "index_chronic_medicines_on_user_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",        limit: 255, default: "", null: false
    t.string   "city_code",   limit: 6,   default: "", null: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "commune",     limit: 255,              null: false
    t.integer  "province_id", limit: 4,                null: false
    t.integer  "district_id", limit: 4,                null: false
  end

  add_index "cities", ["city_code"], name: "index_cities_on_city_code", using: :btree
  add_index "cities", ["district_id"], name: "index_cities_on_district_id", using: :btree
  add_index "cities", ["name"], name: "index_cities_on_name", using: :btree
  add_index "cities", ["province_id"], name: "index_cities_on_province_id", using: :btree

  create_table "diseases", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "districts", force: :cascade do |t|
    t.string   "name",        limit: 255, null: false
    t.integer  "province_id", limit: 4,   null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "districts", ["province_id"], name: "index_districts_on_province_id", using: :btree

  create_table "doctors", force: :cascade do |t|
    t.string   "degree",            limit: 255, default: "", null: false
    t.string   "specialization",    limit: 255, default: "", null: false
    t.string   "doctorRightNumber", limit: 7,                null: false
    t.date     "employmentDate",                             null: false
    t.date     "dismissalDate"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "examinationpositions", force: :cascade do |t|
    t.string   "name",           limit: 255, null: false
    t.string   "result",         limit: 255, null: false
    t.string   "norm",           limit: 255, null: false
    t.integer  "examination_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "examinationpositions", ["examination_id"], name: "index_examinationpositions_on_examination_id", using: :btree

  create_table "examinations", force: :cascade do |t|
    t.string   "name",        limit: 255,                 null: false
    t.datetime "date",                                    null: false
    t.string   "description", limit: 255,                 null: false
    t.boolean  "isconfirmed", limit: 1,   default: false, null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "nurse_id",    limit: 4
    t.integer  "patient_id",  limit: 4
  end

  add_index "examinations", ["nurse_id"], name: "index_examinations_on_nurse_id", using: :btree
  add_index "examinations", ["patient_id"], name: "index_examinations_on_patient_id", using: :btree

  create_table "medical_visits", force: :cascade do |t|
    t.datetime "startTime",                                      null: false
    t.datetime "endTime",                                        null: false
    t.boolean  "ended",                limit: 1, default: false, null: false
    t.integer  "visit_description_id", limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "doctor_id",            limit: 4
    t.integer  "patient_id",           limit: 4
  end

  add_index "medical_visits", ["doctor_id"], name: "fk_rails_9ee3dde3a4", using: :btree
  add_index "medical_visits", ["patient_id"], name: "fk_rails_3af07f757e", using: :btree
  add_index "medical_visits", ["visit_description_id"], name: "index_medical_visits_on_visit_description_id", using: :btree

  create_table "medicines", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "composition",    limit: 255
    t.string   "form",           limit: 255
    t.string   "category",       limit: 255
    t.string   "specialization", limit: 255
    t.string   "effect",         limit: 255
    t.string   "advice",         limit: 10000
    t.string   "dose",           limit: 10000
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "nfz_branches", force: :cascade do |t|
    t.string   "identifier", limit: 255, null: false
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "nurses", force: :cascade do |t|
    t.string   "degree",           limit: 255, default: "", null: false
    t.string   "specialization",   limit: 255, default: "", null: false
    t.string   "nurseRightNumber", limit: 7,                null: false
    t.date     "employmentDate",                            null: false
    t.date     "dismissalDate"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "insurancenumber",  limit: 255, null: false
    t.date     "registrationdate",             null: false
    t.integer  "nfzbranch_id",     limit: 4
    t.integer  "maindoctor_id",    limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "patients", ["maindoctor_id"], name: "index_patients_on_maindoctor_id", using: :btree
  add_index "patients", ["nfzbranch_id"], name: "index_patients_on_nfzbranch_id", using: :btree

  create_table "prescription_positions", force: :cascade do |t|
    t.string   "medicine",        limit: 255, null: false
    t.string   "dose",            limit: 255, null: false
    t.string   "usage",           limit: 255
    t.string   "type",            limit: 255
    t.integer  "prescription_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "prescription_positions", ["prescription_id"], name: "index_prescription_positions_on_prescription_id", using: :btree

  create_table "prescriptions", force: :cascade do |t|
    t.integer  "number",           limit: 4,                 null: false
    t.datetime "drawDate",                                   null: false
    t.datetime "realizationDate",                            null: false
    t.boolean  "isConfirmed",      limit: 1, default: false, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "doctor_id",        limit: 4
    t.integer  "patient_id",       limit: 4
    t.integer  "medical_visit_id", limit: 4
  end

  add_index "prescriptions", ["doctor_id"], name: "fk_rails_f06942fc64", using: :btree
  add_index "prescriptions", ["medical_visit_id"], name: "index_prescriptions_on_medical_visit_id", using: :btree
  add_index "prescriptions", ["patient_id"], name: "fk_rails_bede94f0a0", using: :btree

  create_table "provinces", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "receptionists", force: :cascade do |t|
    t.string   "position",       limit: 255, default: "", null: false
    t.date     "employmentDate",                          null: false
    t.date     "dismissalDate"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "number",     limit: 255, default: "", null: false
    t.string   "name",       limit: 255, default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.datetime "startTime",             null: false
    t.datetime "endTime",               null: false
    t.integer  "interval",    limit: 4, null: false
    t.datetime "intervalEnd"
    t.integer  "room_id",     limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "doctor_id",   limit: 4
  end

  add_index "schedules", ["room_id"], name: "index_schedules_on_room_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255, default: "",    null: false
    t.string   "surname",                limit: 255, default: "",    null: false
    t.integer  "sex",                    limit: 4,   default: 0,     null: false
    t.string   "login",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "email",                  limit: 255, default: "",    null: false
    t.integer  "city_id",                limit: 4,                   null: false
    t.string   "street",                 limit: 255, default: "",    null: false
    t.string   "address",                limit: 255, default: "",    null: false
    t.string   "pesel",                  limit: 11,  default: "",    null: false
    t.string   "phone",                  limit: 12,  default: "",    null: false
    t.boolean  "isactive",               limit: 1,   default: false, null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_id",               limit: 4
    t.integer  "doctor_id",              limit: 4
    t.integer  "nurse_id",               limit: 4
    t.integer  "receptionist_id",        limit: 4
    t.integer  "patient_id",             limit: 4
  end

  add_index "users", ["admin_id"], name: "index_users_on_admin_id", using: :btree
  add_index "users", ["city_id"], name: "index_users_on_city_id", using: :btree
  add_index "users", ["doctor_id"], name: "index_users_on_doctor_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["nurse_id"], name: "index_users_on_nurse_id", using: :btree
  add_index "users", ["patient_id"], name: "index_users_on_patient_id", using: :btree
  add_index "users", ["receptionist_id"], name: "index_users_on_receptionist_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "visit_descriptions", force: :cascade do |t|
    t.string   "symptoms",        limit: 255
    t.string   "examDescription", limit: 255
    t.string   "diagnosis",       limit: 255
    t.string   "treatment",       limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "disease_id",      limit: 4
  end

  add_index "visit_descriptions", ["disease_id"], name: "index_visit_descriptions_on_disease_id", using: :btree

  add_foreign_key "alergic_medicines", "users"
  add_foreign_key "chronic_diseases", "users"
  add_foreign_key "chronic_medicines", "users"
  add_foreign_key "cities", "districts"
  add_foreign_key "cities", "provinces"
  add_foreign_key "districts", "provinces"
  add_foreign_key "examinationpositions", "examinations"
  add_foreign_key "medical_visits", "users", column: "doctor_id"
  add_foreign_key "medical_visits", "users", column: "patient_id"
  add_foreign_key "medical_visits", "visit_descriptions"
  add_foreign_key "patients", "doctors", column: "maindoctor_id"
  add_foreign_key "patients", "nfz_branches", column: "nfzbranch_id"
  add_foreign_key "prescription_positions", "prescriptions"
  add_foreign_key "prescriptions", "medical_visits"
  add_foreign_key "prescriptions", "users", column: "doctor_id"
  add_foreign_key "prescriptions", "users", column: "patient_id"
  add_foreign_key "schedules", "rooms"
  add_foreign_key "users", "admins"
  add_foreign_key "users", "cities"
  add_foreign_key "users", "doctors"
  add_foreign_key "users", "nurses"
  add_foreign_key "users", "patients"
  add_foreign_key "users", "receptionists"
  add_foreign_key "visit_descriptions", "diseases"
end
