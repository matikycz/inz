class ExaminationpositionsController < ApplicationController
  def index
  end

  def show
  end

  def new
    begin
      @examination = Examination.find(params[:examination_id])
      @examinationposition = Examinationposition.new
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego badania!"
      redirect_to examinations_index_path
    end
  end

  def create
    begin
      @examination = Examination.find(params[:examination_id])
      @examinationposition = Examinationposition.new(examinationposition_param)
      @examinationposition.examination = @examination
      if @examinationposition.save
        flash[:success] = "Pomyślnie zapisano pozycję"
        redirect_to examinations_edit_path(@examination)
      else
        flash[:warning] = "Podano nieprawidłowe dane"
        render :new
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego badania!"
      redirect_to examinations_index_path
    end
  end

  def edit
    begin
      @examination = Examination.find(params[:examination_id])
      @examinationposition = Examinationposition.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego badania!"
      redirect_to examinations_index_path
    end
  end

  def update
    begin
      @examination = Examination.find(params[:examination_id])
      @examinationposition = Examinationposition.find(params[:id])
      if @examinationposition.update(examinationposition_param)
        flash[:success] = "Pomyślnie zaktualizowano pozycję"
        redirect_to examinations_edit_path(@examination)
      else
        flash[:warning] = "Podano nieprawidłowe dane"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego badania!"
      redirect_to examinations_index_path
    end
  end

  def destroy
    begin
      @examination = Examination.find(params[:examination_id])
      @examinationposition = Examinationposition.find(params[:id])
      if @examinationposition.delete
        flash[:success] = "Pomyślnie usunięto pozycję"
        redirect_to examinations_edit_path(@examination)
      else
        flash[:warning] = "Błąd podczas usuwania"
        redirect_to examinations_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego badania!"
      redirect_to examinations_index_path
    end
  end

  private
  def examinationposition_param
    params.require(:examinationposition).permit(:name, :result, :norm)
  end
end
