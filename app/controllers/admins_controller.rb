class AdminsController < ApplicationController
  helper_method :sort_column, :sort_direction
  autocomplete :city, :name, :display_value => :full_name, :extra_data => [:city_code], :full => true

  def index
    @users = User.joins(:admin)
                 .search(params[:search])
                 .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                 .paginate(:page => params[:page], :per_page => 20)
  end

  def show
    begin
      @user = User.find(params[:format])
      if !@user.admin?
        flash[:alert] = "Wybrany użytkownik nie jest administratorem"
        redirect_to admins_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego administratora!"
      redirect_to admins_index_path
    end
  end

  def new
    @user = User.new
    @user.admin = Admin.new
  end

  def create
    @user = User.new(user_params)
    @user.isactive = true
    @user.password = generate_password
    @user.city = user_city
    admin = Admin.new(admin_params)
    @user.admin = admin
    @user.sex = params[:sex]
    if @user.save
      UserNotifier.admin_registration(@user).deliver_now
      flash[:notice] = "Udało się pomyślnie zarejstrować nowego administratora."
      redirect_to admins_index_path
    else
      flash[:error] = "error"
      render :new
    end
  end

  def edit
    begin
      @user = User.find(params[:format])
      if !@user.admin?
        flash[:alert] = "Wybrany użytkownik nie jest administratorem"
        redirect_to admins_index_path
      end
      @admin = @user.admin
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego administratora!"
      redirect_to admins_index_path
    end
  end

  def update
    begin
      @user = User.find(params[:format])
      if !@user.admin?
        flash[:alert] = "Wybrany użytkownik nie jest administratorem"
        redirect_to admins_index_path
      end
      @admin = @user.admin
      @user.sex = params[:sex]
      @user.city = user_city
      @admin.update(admin_params)
      if @user.update(user_params)
        flash[:notice] = "Udało się pomyślnie edytować administratora."
        redirect_to admins_index_path
      else
        flash[:error] = "error"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej administratora!"
      redirect_to admins_index_path
    end
  end

  def destroy
    begin
      @user = User.find(params[:format])
      if !@user.admin?
        flash[:alert] = "Wybrany użytkownik nie jest administratorem"
        redirect_to admins_index_path
      end
      @user.isactive = false
      if @user.save
        flash[:notice] = "Udało się pomyślnie usunąć administratora."
        redirect_to admins_index_path
      else
        flash[:error] = "Błąd podczas usuania administratora"
        redirect_to admins_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego administratora!"
      redirect_to admins_index_path
    end
  end

  def get_autocomplete_items(parameters)
    City.where("name like ? or city_code like ? or concat(name, ' ', city_code) like ? or concat(name, ' (', city_code, ')') like ?",
      "%#{parameters[:term]}%", "%#{parameters[:term]}%", "%#{parameters[:term]}%", "%#{parameters[:term]}%").limit(1000)
  end

  private
  def admin_params
    params.require(:user).require(:admin_attributes).permit(:ismain)
  end

  def user_params
    params.require(:user).permit(:name, :surname, :login, :email, :street, :address, :pesel, :phone, :sex)
  end

  def user_city
    City.find(params.require(:post)[:city_id]) unless params.require(:post)[:city_id].empty?
  end

  def generate_password
    password = 8.times.map{([*('a'..'z')]).sample}.join
    first = rand(8)
    secound = rand(8)
    while first == secound do
      secound = rand(8)
    end
    password[first] = password[first].upcase
    password[secound] = rand(10).to_s
    password
  end

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : (Admin.column_names.include?(params[:sort]) ? params[:sort] : "")
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
