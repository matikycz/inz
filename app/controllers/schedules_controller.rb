class SchedulesController < ApplicationController
  helper_method :sort_column, :sort_direction

  def index
    if current_user.admin? or current_user.receptionist?
      @schedules = Schedule.joins(:doctor)
                           .search(params[:search])
                           .order('`' + (sort_column.empty? ? "id" : sort_column) + '` ' + sort_direction)
                           .paginate(:page => params[:page], :per_page => 20)
    elsif current_user.doctor?
      @schedules = Schedule.joins(:doctor)
                           .where(doctor: current_user)
                           .search(params[:search])
                           .order('`' + (sort_column.empty? ? "id" : sort_column) + '` ' + sort_direction)
                           .paginate(:page => params[:page], :per_page => 20)
    else
      @schedules = Schedule.joins(:doctor)
                           .search(params[:search])
                           .order('`' + (sort_column.empty? ? "id" : sort_column) + '` ' + sort_direction)
                           .paginate(:page => params[:page], :per_page => 20)
    end
  end

  def show
    begin
      @schedule = Schedule.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wpisu!"
      redirect_to schedules_index_path
    end
  end

  def new
    @schedule = Schedule.new
  end

  def create
    @schedule = Schedule.new(schedule_params)
    @schedule.doctor = User.find(params[:schedule][:doctor])
    @schedule.room = Room.find(params[:schedule][:room])
    if not Schedule.validate_schedule(@schedule)
      flash[:alert] = "Nie możesz dodać takiej pozycji, ponieważ pokrywa się z inną"
      render :new
    elsif @schedule.save
      flash[:success] = "Pomyślnie dodano wpis do grafiku"
      redirect_to schedules_index_path
    else
      flash[:error] = "error"
      render :new
    end
  end

  def edit
    begin
      @schedule = Schedule.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wpisu!"
      redirect_to schedules_index_path
    end
  end

  def update
    begin
      @schedule = Schedule.find(params[:format])
      @schedule.doctor = User.find(params[:schedule][:doctor])
      @schedule.room = Room.find(params[:schedule][:room])
      if @schedule.update(schedule_params)
        flash[:success] = "Pomyślnie zaktualizowano wpis"
        redirect_to schedules_index_path
      else
        flash[:error] = "error"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wpisu!"
      redirect_to schedules_index_path
    end
  end

  def destroy
    begin
      @schedule = Schedule.find(params[:format])
      @schedule.destroy
      flash[:success] = "Pomyślnie usunięto wpis"
      redirect_to schedules_index_path
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wpisu!"
      redirect_to schedules_index_path
    end
  end

  private
  def schedule_params
    params.require(:schedule).permit(:startTime, :endTime, :interval, :intervalEnd)
  end

  def sort_column
    Schedule.column_names.include?(params[:sort]) ? params[:sort] : (User.column_names.include?(params[:sort]) ? params[:sort] : "")
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
