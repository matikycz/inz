class MedicinesController < ApplicationController
	layout 'empty'
  def show
  	begin
  		@medicine = Medicine.find(params[:format])
  	rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego leku!"
      redirect_to medical_visits_index_path
    end
  end

  def info
  	@field = params[:field]
  	@medicine = Medicine.where(name: params[:query]).first
  end
end
