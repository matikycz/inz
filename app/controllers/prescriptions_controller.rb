class PrescriptionsController < ApplicationController
  def index
  end

  def show
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      @prescription = Prescription.find(params[:id])
      @position1 = get_position(@prescription.prescription_positions, 1)
      @position2 = get_position(@prescription.prescription_positions, 2)
      @position3 = get_position(@prescription.prescription_positions, 3)
      @position4 = get_position(@prescription.prescription_positions, 4)
      @position5 = get_position(@prescription.prescription_positions, 5)

      respond_to do |format|
      format.html
      format.pdf do
        render pdf: "recepta",
        # page_size:  'Letter'
        page_height:'215mm',
        page_width: '110mm',
        margin:  {  top:    3,                     # default 10 (mm)
                    bottom: 3,
                    left:   3,
                    right:  3},
        encoding:   'UTF-8'
      end
    end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej recepty!"
      redirect_to medical_visits_index_path
    end
  end

  def new
    @medical_visit = MedicalVisit.find(params[:medical_visit_id])
    @prescription = Prescription.new
    @position1 = PrescriptionPosition.new
    @position2 = PrescriptionPosition.new
    @position3 = PrescriptionPosition.new
    @position4 = PrescriptionPosition.new
    @position5 = PrescriptionPosition.new
  end

  def create
    @medical_visit = MedicalVisit.find(params[:medical_visit_id])
    @prescription = Prescription.new(prescription_params)
    @prescription.doctor = @medical_visit.doctor
    @prescription.patient = @medical_visit.patient
    @prescription.medical_visit = @medical_visit
    @prescription.drawDate = DateTime.now
    @position1 = PrescriptionPosition.new(position1_params)
    @position2 = PrescriptionPosition.new(position2_params)
    @position3 = PrescriptionPosition.new(position3_params)
    @position4 = PrescriptionPosition.new(position4_params)
    @position5 = PrescriptionPosition.new(position5_params)
    @position1.prescription = @prescription
    @position2.prescription = @prescription
    @position3.prescription = @prescription
    @position4.prescription = @prescription
    @position5.prescription = @prescription
    if validate
      if params[:commit] == "Zapisz receptę"
        @prescription.isConfirmed = false
      else
        @prescription.isConfirmed = true
      end
      if @prescription.save
        @position1.save if fill_position1
        @position2.save if fill_position2
        @position3.save if fill_position3
        @position4.save if fill_position4
        @position5.save if fill_position5
        flash[:success]
        redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
      else
        flash[:alert] = "Błąd podczas zapisu recepty"
        render :new
      end
    else
      flash[:alert] = "Podałeś błędne dane"
      render :new
    end
  end

  def edit
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      @prescription = Prescription.find(params[:id])
      if(@medical_visit.ended or @prescription.isConfirmed or @medical_visit.doctor != current_user)
        flash[:alert] = "Nie możesz edytować tej recepty"
        redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
      else
        @position1 = get_position(@prescription.prescription_positions, 1)
        @position2 = get_position(@prescription.prescription_positions, 2)
        @position3 = get_position(@prescription.prescription_positions, 3)
        @position4 = get_position(@prescription.prescription_positions, 4)
        @position5 = get_position(@prescription.prescription_positions, 5)
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej recepty!"
      redirect_to medical_visits_index_path
    end
  end

  def update
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      @prescription = Prescription.find(params[:id])
      if((@medical_visit.ended or @prescription.isConfirmed or @medical_visit.doctor != current_user) and !current_user.admin?)
        flash[:alert] = "Nie możesz edytować tej recepty"
        redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
      else
        if validate
          @position1 = get_position(@prescription.prescription_positions, 1)
          @position2 = get_position(@prescription.prescription_positions, 2)
          @position3 = get_position(@prescription.prescription_positions, 3)
          @position4 = get_position(@prescription.prescription_positions, 4)
          @position5 = get_position(@prescription.prescription_positions, 5)
          if !@position1.id.nil?
            @position1.update(position1_params)
          elsif fill_position1
            @position1 = PrescriptionPosition.new(position1_params)
            @position1.prescription = @prescription
            @position1.save
          end
          if !@position2.id.nil?
            @position2.update(position2_params)
          elsif fill_position2
            @position2 = PrescriptionPosition.new(position2_params)
            @position2.prescription = @prescription
            @position2.save
          end
          if !@position3.id.nil?
            @position3.update(position3_params)
          elsif fill_position3
            @position3 = PrescriptionPosition.new(position3_params)
            @position3.prescription = @prescription
            @position3.save
          end
          if !@position4.id.nil?
            @position4.update(position4_params)
          elsif fill_position4
            @position4 = PrescriptionPosition.new(position4_params)
            @position4.prescription = @prescription
            @position4.save
          end
          if !@position5.id.nil?
            @position5.update(position5_params)
          elsif fill_position5
            @position5 = PrescriptionPosition.new(position5_params)
            @position5.prescription = @prescription
            @position5.save
          end

          if params[:commit] == "Zapisz receptę"
            @prescription.isConfirmed = false
          else
            @prescription.isConfirmed = true
          end
          @prescription.number = prescription_params[:number]
          @prescription.realizationDate = prescription_params[:realizationDate]
          if @prescription.save
            flash[:success] = "Pomyślnie zaktualizowano receptę"
            redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
          else
            flash[:alert] = "Nieprawidłowe dane"
            redner :edit
          end
        else
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej recepty!"
      redirect_to medical_visits_index_path
    end
  end

  def destroy
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      @prescription = Prescription.find(params[:id])
      if((@medical_visit.ended or @prescription.isConfirmed or @medical_visit.doctor != current_user) and !current_user.admin?)
        flash[:alert] = "Nie możesz usunąć tej recepty"
        redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
      else
        if @prescription.destroy
          flash[:success] = "Pomyślnie usunięto receptę"
          redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
        else
          flash[:alert] = "Nie możesz usunąć tej recepty"
          redirect_to edit_medical_visit_visit_description_path(@medical_visit, @medical_visit.visit_description)
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej recepty!"
      redirect_to medical_visits_index_path
    end
  end

  def print
    @text = 'test'
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "file_name",   # Excluding ".pdf" extension.
              template: 'prescriptions/print.pdf.erb',
              layout: 'pdf'
      end
    end
  end

  private
  def prescription_params
    params.require(:prescription).permit(:number, :realizationDate)
  end

  def position1_params
    params.require(:prescription).require(:position1).permit(:medicine, :dose, :usage, :type)
  end

  def position2_params
    params.require(:prescription).require(:position2).permit(:medicine, :dose, :usage, :type)
  end

  def position3_params
    params.require(:prescription).require(:position3).permit(:medicine, :dose, :usage, :type)
  end

  def position4_params
    params.require(:prescription).require(:position4).permit(:medicine, :dose, :usage, :type)
  end

  def position5_params
    params.require(:prescription).require(:position5).permit(:medicine, :dose, :usage, :type)
  end

  def fill_position1
    position1_params[:medicine].present?
  end

  def fill_position2
    position2_params[:medicine].present?
  end

  def fill_position3
    position3_params[:medicine].present?
  end

  def fill_position4
    position4_params[:medicine].present?
  end

  def fill_position5
    position5_params[:medicine].present?
  end

  def validate
    if (fill_position1 && !fill_position2 && !fill_position3 && !fill_position4 && !fill_position5) ||
       (fill_position1 && fill_position2 && !fill_position3 && !fill_position4 && !fill_position5) ||
       (fill_position1 && fill_position2 && fill_position3 && !fill_position4 && !fill_position5) ||
       (fill_position1 && fill_position2 && fill_position3 && fill_position4 && !fill_position5) ||
       (fill_position1 && fill_position2 && fill_position3 && fill_position4 && fill_position5)
      true
    else
      false
    end
  end

  def get_position(list, position)
    if(list.size >= position)
      list[position - 1]
    else
      PrescriptionPosition.new
    end
  end
end
