class AlergicMedicinesController < ApplicationController
  def index
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicines = @user.alergic_medicines
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def show
  end

  def new
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicine = AlergicMedicine.new
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def create
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicine = AlergicMedicine.new(alergic_medicine_param)
        @alergic_medicine.user = @user
        if @alergic_medicine.save
          flash[:success] = "Pomyślnie dodano lek alergenny!"
          redirect_to patients_show_path(@user)
        else
          flash[:success] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def edit
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicine = AlergicMedicine.find(params[:id])
      else
        flash[:success] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def update
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicine = AlergicMedicine.find(params[:id])
        if @alergic_medicine.update(alergic_medicine_param)
          flash[:success] = "Pomyślnie zaktualizowano lek alergenny!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def destroy
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @alergic_medicine = AlergicMedicine.find(params[:id])
        if @alergic_medicine.delete
          flash[:success] = "Pomyślnie usuniętą lek alergenny!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Błąd usuwania!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  private
  def alergic_medicine_param
    params.require(:alergic_medicine).permit(:name)
  end
end