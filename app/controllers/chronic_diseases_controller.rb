class ChronicDiseasesController < ApplicationController
  def index
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_diseases = @user.chronic_diseases
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def show
  end

  def new
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_disease = ChronicDisease.new
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def create
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_disease = ChronicDisease.new(chronic_disease_param)
        @chronic_disease.user = @user
        if @chronic_disease.save
          flash[:success] = "Pomyślnie dodano chorobę przewlekłą!"
          redirect_to patients_show_path(@user)
        else
          flash[:success] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def edit
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_disease = ChronicDisease.find(params[:id])
      else
        flash[:success] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def update
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_disease = ChronicDisease.find(params[:id])
        if @chronic_disease.update(chronic_disease_param)
          flash[:success] = "Pomyślnie zaktualizowano chorobę przewlekłą!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def destroy
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_disease = ChronicDisease.find(params[:id])
        if @chronic_disease.delete
          flash[:success] = "Pomyślnie usuniętą chorobę przewlekłą!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Błąd usuwania!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  private
  def chronic_disease_param
    params.require(:chronic_disease).permit(:name)
  end
end
