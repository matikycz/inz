class RoomsController < ApplicationController
  helper_method :sort_column, :sort_direction

  def index
    @rooms = Room.all
                 .search(params[:search])
                 .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                 .paginate(:page => params[:page], :per_page => 20)
  end

  def show
    begin
      @room = Room.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego gabinetu!"
      redirect_to rooms_index_path
    end
  end

  def new
    @room = Room.new
  end

  def create
    @room = Room.new(room_params)
    if @room.save
      flash[:success] = "Pomyślnie dodano gabinet"
      redirect_to rooms_index_path
    else
      flash[:error] = "error"
      render :new
    end
  end

  def edit
    begin
      @room = Room.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego gabinetu!"
      redirect_to rooms_index_path
    end
  end

  def update
    @room = Room.find(params[:format])
    if @room.update(room_params)
      flash[:success] = "Pomyślnie zaktualizowano gabinet"
      redirect_to rooms_index_path
    else
      flash[:error] = "error"
      render :edit
    end
  end

  def destroy
    begin
      @room = Room.find(params[:format])
      @room.destroy
      flash[:success] = "Pomyślnie usunięto gabinet"
      redirect_to rooms_index_path
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie można usunąć tego gabinetu!"
      redirect_to rooms_index_path
    end
  end

  private
  def room_params
     params.require(:room).permit(:name, :number)
  end

  def sort_column
    Room.column_names.include?(params[:sort]) ? params[:sort] : ""
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
