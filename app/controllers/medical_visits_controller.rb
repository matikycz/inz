class MedicalVisitsController < ApplicationController
  helper_method :sort_column, :sort_direction
  autocomplete :patient, :name, :display_value => :patient_name, :extra_data => [:surname, :pesel], :full => true

  def index
    if current_user.doctor? and params[:format]
      @medical_visits = MedicalVisit.joins(:doctor)
                                    .where(patient: User.find(params[:format]))
                                    .where(ended: 1)
                                    .order('`' + (sort_column.empty? ? "startTime" : sort_column) + '` ' + sort_direction)
                                    .paginate(:page => params[:page1], :per_page => 20)
    elsif current_user.doctor?
      date = params[:date].present? ? Time.parse(params[:date]) : Time.now
      @medical_visits = MedicalVisit.joins(:patient)
                                    .where(doctor: current_user)
                                    .where("date(startTime) = :date",  date: "#{date.to_date}")
                                    .order('`' + (sort_column.empty? ? "startTime" : sort_column) + '` ' + sort_direction)
                                    .paginate(:page => params[:page], :per_page => 20)
    elsif current_user.patient?
      @medical_visits = MedicalVisit.joins(:doctor)
                                    .where(patient: current_user)
                                    .where("date(startTime) >= :date",  date: "#{Time.now.to_date}")
                                    .order('`' + (sort_column.empty? ? "startTime" : sort_column) + '` ' + sort_direction)
                                    .paginate(:page => params[:page1], :per_page => 10)
      @ended_visits = MedicalVisit.joins(:doctor)
                                  .where(patient: current_user)
                                  .where("date(startTime) < :date",  date: "#{Time.now.to_date}")
                                  .order('`' + (sort_column.empty? ? "startTime" : sort_column) + '` ' + sort_direction)
                                  .paginate(:page => params[:page2], :per_page => 10)
    else
      @medical_visits = MedicalVisit.where("date(startTime) >= :date",  date: "#{Time.now.to_date}")
                                    .order('`' + (sort_column.empty? ? "startTime" : sort_column) + '` ' + sort_direction)
                                    .paginate(:page => params[:page], :per_page => 20)
    end
  end

  def show
    begin
      @medical_visit = MedicalVisit.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def new
    @medical_visit = MedicalVisit.new
    if current_user.patient?
      @doctor = current_user.patient.maindoctor.user
    else
      @doctor = User.where.not(doctor: nil).first
    end
    @date = Time.now
    @visits = visits_to_display(@doctor, @date)
  end

  def create
    @medical_visit = MedicalVisit.new(medical_visit_params)
    @medical_visit.doctor = doctor_param
    @medical_visit.endTime = @medical_visit.startTime + 15*60 if @medical_visit.startTime
    if current_user.patient?
      @medical_visit.patient = current_user
    else
      @medical_visit.patient = patient_param
    end
    unless validate @medical_visit and @medical_visit.startTime > Time.now
      @doctor = doctor_param
      @date = @medical_visit.startTime ? @medical_visit.startTime : DateTime.now
      @visits = visits_to_display(@doctor, @date)
      flash[:alert] = "Nie możesz zarejestrować wizyty w tym terminie"
      render :new
    else
      if @medical_visit.save
        flash[:notice] = "Udało się pomyślnie zarejestrować nową wizytę"
        redirect_to medical_visits_index_path
      else
        @doctor = doctor_param
        @date = @medical_visit.startTime
        @visits = visits_to_display(@doctor, @date)
        flash[:error] = "error"
        render :new
      end
    end
  end

  def edit
    begin
      @medical_visit = MedicalVisit.find(params[:format])
      if current_user.patient?
        if @medical_visit.patient != current_user or @medical_visit.startTime < Time.now
          flash[:alert] = "Nie możesz edytować tej wizyty!"
          redirect_to medical_visits_index_path
        else
          @doctor = @medical_visit.doctor
          @date = @medical_visit.startTime
          @visits = visits_to_display(@doctor, @date)
          render :edit
        end
      else
        if @medical_visit.startTime < Time.now
          flash[:alert] = "Nie możesz edytować tej wizyty!"
          redirect_to medical_visits_index_path
        else
          @doctor = @medical_visit.doctor
          @date = @medical_visit.startTime
          @visits = visits_to_display(@doctor, @date)
          render :edit
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def update
    begin
      @medical_visit = MedicalVisit.find(params[:format])
      @medical_visit.startTime = Time.parse(params.require(:medical_visit).permit(:startTime).to_s)
      @medical_visit.doctor = doctor_param
      @medical_visit.endTime = @medical_visit.startTime + 15*60 if @medical_visit.startTime
      if current_user.patient?
        @medical_visit.patient = current_user
      else
        @medical_visit.patient = patient_param
      end
      unless validate @medical_visit or @medical_visit.startTime > Time.now
        @doctor = doctor_param
        @date = @medical_visit.startTime ? @medical_visit.startTime : DateTime.now
        @visits = visits_to_display(@doctor, @date)
        flash[:alert] = "Nie możesz zarejestrować wizyty w tym terminie"
        render :edit
      else
        if @medical_visit.save
          flash[:notice] = "Pomyślnie zmodyfikowano wizytę"
          redirect_to medical_visits_index_path
        else
          @doctor = doctor_param
          @date = @medical_visit.startTime
          @visits = visits_to_display(@doctor, @date)
          flash[:error] = "error"
          render :edit
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def destroy
    begin
      @medical_visit = MedicalVisit.find(params[:format])
      if @medical_visit.startTime > Time.now and (current_user.receptionist? or current_user = @medical_visit.patient) and @medical_visit.destroy
        flash[:notice] = "Pomyslnie wizytę"
        redirect_to medical_visits_index_path
      else
        flash[:alert] = "Nie możesz usunąć tej wizyty"
        redirect_to medical_visits_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def doctor_visits
    @doctor = User.find(params.require(:medical_visit)[:doctor])
    @date = params.require(:medical_visit)[:startTime].empty? ? DateTime.now : DateTime.parse(params.require(:medical_visit)[:startTime])
    @visits = visits_to_display(@doctor, @date)
  end

  def patient_change
    @patient = User.find(params.require(:post)[:patient_id]) if params.require(:post)[:patient_id].present?
    @doctor = !@patient.nil? ? @patient.patient.maindoctor.user :  User.find(params.require(:medical_visit)[:doctor])
    @date = params.require(:medical_visit)[:startTime].empty? ? DateTime.now : DateTime.parse(params.require(:medical_visit)[:startTime])
    @visits = visits_to_display(@doctor, @date)
  end

  def get_autocomplete_items(parameters)
    User.where("patient_id is not null and isactive = 1 and (name like :query or surname like :query or pesel like :query or CONCAT_WS(' ', name, surname, pesel) like :query)", query: "%#{parameters[:term]}%")
  end

  private
  def medical_visit_params
    params.require(:medical_visit).permit(:startTime)
  end

  def doctor_param
    User.find(params.require(:medical_visit)[:doctor]) unless params.require(:medical_visit)[:doctor].empty?
  end

  def patient_param
    User.find(params.require(:post)[:patient_id]) unless params.require(:post)[:patient_id].empty?
  end

  def validate medical_visit
    enable = false
    unless medical_visit.startTime
      enable = false
    else
      schedules = Schedule.where(doctor: doctor_param)
      schedules.each { |schedule|
        if schedule.interval == 0
          enable = (enable or
                   (schedule.startTime <= medical_visit.startTime and
                   schedule.endTime >= (medical_visit.startTime + 15*60) and
                   MedicalVisit.where(startTime: medical_visit.startTime).where(doctor: medical_visit.doctor).count() == 0))
        else
          enable = (enable or
                   (schedule.startTime.wday == medical_visit.startTime.wday and
                   schedule.startTime.utc.strftime( "%H%M" ) <= medical_visit.startTime.utc.strftime( "%H%M" ) and
                   schedule.endTime.utc.strftime( "%H%M" ) >= (medical_visit.startTime + 15*60).utc.strftime( "%H%M" ) and
                   MedicalVisit.where(startTime: medical_visit.startTime).where(doctor: medical_visit.doctor).count() == 0))
        end
      }
    end
    enable
  end

  Visit = Struct.new(:start, :end, :status)

  def visits_to_display(doctor, date)
    visits = []
    Schedule.doctor_visits(doctor, date).each { |schedule|
      if schedule.startTime.to_date == date.to_date
        startTime = schedule.startTime
        puts date.to_date
        endTime = schedule.endTime
      else
        startTime = Time.parse(date.strftime("%Y-%m-%d").to_s + " " +schedule.startTime.strftime("%H:%M").to_s)
        endTime = Time.parse(date.strftime("%Y-%m-%d").to_s + " " +schedule.endTime.to_time.strftime("%H:%M").to_s)
      end
      while startTime < endTime
        result = MedicalVisit.where(doctor: doctor).where(startTime: startTime)
        v = Visit.new
        v.start = startTime
        v.end = startTime + 15*60
        if result.count() == 0
          v.status = "wolne"
        else
          v.status = current_user.patient? ? (result.first.patient == current_user ? "Twoja wizyta" : "zajęte") : result.first.patient.name + " " + result.first.patient.surname
        end
        visits.push v
        startTime += 15*60
      end
    }
    visits
  end

  def sort_column
    MedicalVisit.column_names.include?(params[:sort]) ? params[:sort] : (User.column_names.include?(params[:sort]) ? params[:sort] : "")
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end