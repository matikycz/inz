class ExaminationsController < ApplicationController
  helper_method :sort_column, :sort_direction
  autocomplete :patient, :name, :display_value => :patient_name, :extra_data => [:surname, :pesel], :full => true
  def index
    if params[:format].present?
      @examinations = Examination.where(patient_id: params[:format])
                                 .joins(:patient)
                                 .search(params[:search])
                                 .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                                 .paginate(:page => params[:page], :per_page => 20)
    elsif current_user.admin? or current_user.nurse? or current_user.doctor?
      @examinations = Examination.joins(:patient)
                                 .search(params[:search])
                                 .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                                 .paginate(:page => params[:page], :per_page => 20)
    else
      @examinations = Examination.where(patient: current_user)
                                 .joins(:patient)
                                 .search(params[:search])
                                 .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                                 .paginate(:page => params[:page], :per_page => 20)
    end
  end

  def show
    begin
      @examination = Examination.find(params[:format])
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wyniku badań!"
      redirect_to examinations_index_path
    end
  end

  def new
    @examination = Examination.new
  end

  def create
    @examination = Examination.new(examination_param)
    @examination.patient = patient_param
    @examination.nurse = current_user
    if params[:commit] == "Zapisz wynik badania"
      if @examination.save
        flash[:success] = "Pomyślnie zapisano wynik badania"
        redirect_to examinations_edit_path(@examination)
      else
        flash[:warning] = "Podano nieprawidłowe dane"
        render :new
      end
    else
      @examination.isconfirmed = true
      if @examination.save
        flash[:success] = "Pomyślnie zatwierdzono wynik badania"
        redirect_to examinations_index_path
      else
        flash[:warning] = "Podano nieprawidłowe dane"
        render :new
      end
    end
  end

  def edit
    begin
      @examination = Examination.find(params[:format])
      if @examination.isconfirmed
        flash[:alert] = "Nie możesz edytować tego wyniku!"
        redirect_to examinations_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wyniku badań!"
      redirect_to examinations_index_path
    end
  end

  def update
    begin
      @examination = Examination.find(params[:format])
      @examination.patient = patient_param
      @examination.nurse = current_user
      if params[:commit] == "Zapisz wynik badania"
        if @examination.update(examination_param)
          flash[:success] = "Pomyślnie zapisano wynik badania"
          redirect_to examinations_edit_path(@examination)
        else
          flash[:warning] = "Podano nieprawidłowe dane"
          render :edit
        end
      else
        @examination.isconfirmed = true
        if @examination.update(examination_param)
          flash[:success] = "Pomyślnie zatwierdzono wynik badania"
          redirect_to examinations_index_path
        else
          flash[:warning] = "Podano nieprawidłowe dane"
          render :edit
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wyniku badań!"
      redirect_to examinations_index_path
    end
  end

  def destroy
    begin
      @examination = Examination.find(params[:format])
      if !@examination.isconfirmed and @examination.destroy
        flash[:success] = "Pomyślnie usunięto wynik badania"
        redirect_to examinations_index_path
      else
        flash[:alert] = "Nie możesz usunąć wyniku badań!"
        redirect_to examinations_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego wyniku badań!"
      redirect_to examinations_index_path
    end
  end

  def get_autocomplete_items(parameters)
    User.where("patient_id is not null and isactive = 1 and (name like :query or surname like :query or pesel like :query or CONCAT_WS(' ', name, surname, pesel) like :query)", query: "%#{parameters[:term]}%")
  end

  private
  def examination_param
    params.require(:examination).permit(:name, :description, :date)
  end

  def patient_param
    User.find(params.require(:post)[:patient_id]) unless params.require(:post)[:patient_id].empty?
  end

  def sort_column
    Examination.column_names.include?(params[:sort]) ? params[:sort] : (User.column_names.include?(params[:sort]) ? params[:sort] : "")
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
