class VisitDescriptionsController < ApplicationController
  def index
  end

  def new
    @medical_visit = MedicalVisit.find(params[:medical_visit_id])
    if @medical_visit.ended
      flash[:alert] = "Nie możesz edytować tej wizyty"
      redirect_to medical_visits_index_path
    else
      if @medical_visit.visit_description.nil?
        @visit_description = VisitDescription.new
      else
        @visit_description = @medical_visit.visit_description
      end
    end
  end

  def create
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      if @medical_visit.ended
        flash[:alert] = "Nie możesz edytować tej wizyty"
        redirect_to medical_visits_index_path
      else
        if @medical_visit.visit_description.nil?
          @visit_description = VisitDescription.new(visit_description_params)
          @visit_description.disease = disease_param
          if @visit_description.save
            @medical_visit.visit_description = @visit_description
            if params[:commit] == "Zapisz wizytę"
              @medical_visit.save
              render :edit
            else
              @medical_visit.ended = true
              @medical_visit.save
              redirect_to medical_visits_index_path
            end
          else
            @medical_visit.save
            flash[:alert] = "Błąd podczas zapisu wizyty"
            render :new
          end
        else
          @visit_description = @medical_visit.visit_description
          if @visit_description.update(visit_description_params)
            if params[:commit] == "Zapisz wizytę"
              render :edit
            else
              @medical_visit.ended = true
              @medical_visit.save
              redirect_to medical_visits_index_path
            end
          else
            flash[:alert] = "Błąd podczas zapisu wizyty"
            render :edit
          end
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def edit
    @medical_visit = MedicalVisit.find(params[:medical_visit_id])
    if @medical_visit.ended
      flash[:alert] = "Nie możesz edytować tej wizyty"
      redirect_to medical_visits_index_path
    else
      if @medical_visit.visit_description.nil?
        @visit_description = VisitDescription.new
      else
        @visit_description = @medical_visit.visit_description
      end
    end
  end

  def update
    begin
      @medical_visit = MedicalVisit.find(params[:medical_visit_id])
      if @medical_visit.ended
        flash[:alert] = "Nie możesz edytować tej wizyty"
        redirect_to medical_visits_index_path
      else
        @visit_description = @medical_visit.visit_description
        @visit_description.disease = disease_param
        if @visit_description.update(visit_description_params)
          if params[:commit] == "Zapisz wizytę"
            render :edit
          else
            @medical_visit.ended = true
            @medical_visit.save
            redirect_to medical_visits_index_path
          end
        else
          flash[:alert] = "Błąd podczas zapisu wizyty"
          render :edit
        end
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiej wizyty!"
      redirect_to medical_visits_index_path
    end
  end

  def delete
  end

  private
  def visit_description_params
    params.require(:visit_description).permit(:symptoms, :examDescription, :diagnosis, :treatment)
  end

  def disease_param
    Disease.find(params.require(:visit_description).permit(:disease)[:disease].to_i)
  end
end