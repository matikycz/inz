class ChronicMedicinesController < ApplicationController
  def index
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicines = @user.chronic_medicines
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def show
  end

  def new
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicine = ChronicMedicine.new
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def create
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicine = ChronicMedicine.new(chronic_medicine_param)
        @chronic_medicine.user = @user
        if @chronic_medicine.save
          flash[:success] = "Pomyślnie dodano stale brany lek!"
          redirect_to patients_show_path(@user)
        else
          flash[:success] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def edit
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicine = ChronicMedicine.find(params[:id])
      else
        flash[:success] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def update
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicine = ChronicMedicine.find(params[:id])
        if @chronic_medicine.update(chronic_medicine_param)
          flash[:success] = "Pomyślnie zaktualizowano stale brany lek!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Podano błędne dane!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def destroy
    begin
      @user = User.find(params[:user_id])
      if @user.patient?
        @chronic_medicine = ChronicMedicine.find(params[:id])
        if @chronic_medicine.delete
          flash[:success] = "Pomyślnie usuniętą stale brany lek!"
          redirect_to patients_show_path(@user)
        else
          flash[:warning] = "Błąd usuwania!"
          render :new
        end
      else
        flash[:alert] = "Nie ma takiego pacjenta!"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  private
  def chronic_medicine_param
    params.require(:chronic_medicine).permit(:name)
  end
end