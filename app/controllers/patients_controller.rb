class PatientsController < ApplicationController
  helper_method :sort_column, :sort_direction
  autocomplete :city, :name, :display_value => :full_name, :extra_data => [:city_code], :full => true

  def index
    if current_user.admin? or current_user.receptionist?
      @users = User.joins(:patient)
                   .search(params[:search])
                   .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                   .paginate(:page => params[:page], :per_page => 20)
    else
      @users = User.joins(:patient)
                   .where(isactive: true)
                   .search(params[:search])
                   .order((sort_column.empty? ? "id" : sort_column) + ' ' + sort_direction)
                   .paginate(:page => params[:page], :per_page => 20)
    end
  end

  def show
    begin
      @user = User.find(params[:format])
      if !@user.patient?
        flash[:alert] = "Wybrany użytkownik nie jest pacjentem"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def new
    @user = User.new
    @user.patient = Patient.new
  end

  def create
    @user = User.new(user_params)
    @user.isactive = true
    @user.password = generate_password
    @user.login = @user.pesel
    @user.city = user_city
    patient = Patient.new(patient_params)
    patient.registrationdate = Time.new
    patient.maindoctor = patient_maindoctor
    patient.nfzbranch = patient_nfzbranch
    @user.patient = patient
    @user.sex = params[:sex]
    if @user.save
      UserNotifier.patient_registration(@user).deliver_now
      flash[:notice] = "Udało się pomyślnie zarejstrować nowego pacjenta."
      redirect_to patients_index_path
    else
      flash[:error] = "error"
      render :new
    end
  end

  def edit
    begin
      @user = User.find(params[:format])
      if !@user.patient?
        flash[:alert] = "Wybrany użytkownik nie jest pacjentem"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def update
    begin
      @user = User.find(params[:format])
      if !@user.patient?
        flash[:alert] = "Wybrany użytkownik nie jest pacjentem"
        redirect_to patients_index_path
      end
      @patient = @user.patient
      @user.sex = params[:sex]
      @user.city = user_city
      @patient.maindoctor = patient_maindoctor
      @patient.nfzbranch = patient_nfzbranch
      @user.pesel = user_params[:pesel]
      @user.login = @user.pesel
      @patient.update(patient_params)
      if @user.update(user_params)
        flash[:notice] = "Udało się pomyślnie edytować pacjenta."
        redirect_to patients_index_path
      else
        flash[:error] = "error"
        render :edit
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def destroy
    begin
      @user = User.find(params[:format])
      if !@user.patient?
        flash[:alert] = "Wybrany użytkownik nie jest pacjentem"
        redirect_to patients_index_path
      end
      @user.isactive = false
      if @user.save
        flash[:notice] = "Udało się pomyślnie usunąć pacjenta."
        redirect_to patients_index_path
      else
        flash[:error] = "Błąd podczas usuania pacjenta"
        redirect_to patients_index_path
      end
    rescue ActiveRecord::RecordNotFound => e
      flash[:alert] = "Nie ma takiego pacjenta!"
      redirect_to patients_index_path
    end
  end

  def get_autocomplete_items(parameters)
    City.where("name like ? or city_code like ? or concat(name, ' ', city_code) like ? or concat(name, ' (', city_code, ')') like ?",
      "%#{parameters[:term]}%", "%#{parameters[:term]}%", "%#{parameters[:term]}%", "%#{parameters[:term]}%").limit(1000)
  end

  private
  def patient_params
    params.require(:user).require(:patient_attributes).permit(:insurancenumber)
  end

  def patient_maindoctor
    Doctor.find(params.require(:user).require(:patient_attributes).permit(:maindoctor)[:maindoctor].to_i)
  end

  def patient_nfzbranch
    NfzBranch.find(params.require(:user).require(:patient_attributes).permit(:nfzbranch)[:nfzbranch].to_i)
  end

  def user_params
    params.require(:user).permit(:name, :surname, :login, :email, :street, :address, :pesel, :phone, :sex)
  end

  def user_city
    City.find(params.require(:post)[:city_id]) unless params.require(:post)[:city_id].empty?
  end

  def generate_password
    password = 8.times.map{([*('a'..'z')]).sample}.join
    first = rand(8)
    secound = rand(8)
    while first == secound do
      secound = rand(8)
    end
    password[first] = password[first].upcase
    password[secound] = rand(10).to_s
    password
  end

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : (Patient.column_names.include?(params[:sort]) ? params[:sort] : "")
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
