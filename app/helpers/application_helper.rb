module ApplicationHelper

 	def bootstrap_class_for(flash_type)
    	case flash_type
      	when "success"
        	"alert-success"   # Green
      	when "error"
        	"alert-danger"    # Red
      	when "alert"
        	"alert-warning"   # Yellow
      	when "notice"
        	"alert-info"      # Blue
      	else
        	flash_type.to_s
    	end
  	end

  def sortable(column)
    column ||= "id"
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
    hash = Hash.new
    hash[:sort] = column
    hash[:direction] = direction
    if params[:search].present?
      hash[:search] = params[:search]
    end
    if params[:date].present?
      hash[:date] = params[:date]
    end
    if params[:category].present?
      hash[:category] = params[:category]
    end
    if params[:resource].present?
      hash[:resource] = params[:resource]
    end
    if params[:payment].present?
      hash[:payment] = params[:payment]
    end
    if params[:type].present?
      hash[:type] = params[:type]
    end
    hash.compact
    hash
  end

  def column_name(name)
    if sort_column == name
      if sort_direction == "asc"
        '<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>'.html_safe
      else
        '<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>'.html_safe
      end
    else
      ""
    end
  end
end
