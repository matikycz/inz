module AdminsHelper
	def get_city_id(city)
    City.where("concat(name, ' (', city_code, ')') like ?", "%#{city.name}%").pluck(:id).first if city
  end
end
