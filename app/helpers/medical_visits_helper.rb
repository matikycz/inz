module MedicalVisitsHelper
	def get_patient_id(patient)
    User.where("concat(name, surname, ' (', pesel, ')') like ?", "%#{patient.name}%").pluck(:id).first if patient
  end
end
