class UserNotifier < ApplicationMailer
	default from: "powiadomienia@sitarczyk.eu"

	def admin_registration(user)
		@user = user
		mail(to: @user.email, subject: 'Rejestracja administratora')
	end

	def doctor_registration(user)
		@user = user
		mail(to: @user.email, subject: 'Rejestracja lekarza')
	end

	def nurse_registration(user)
		@user = user
		mail(to: @user.email, subject: 'Rejestracja pielęgniarki')
	end

	def receptionist_registration(user)
		@user = user
		mail(to: @user.email, subject: 'Rejestracja recepcjonistki')
	end

	def patient_registration(user)
		@user = user
		mail(to: @user.email, subject: 'Rejestracja pacjenta')
	end
end
