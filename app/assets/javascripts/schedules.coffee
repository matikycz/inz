# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:load ready', ->
	if $('#scheduleinterval').is(':checked')
		$("#scheduleintervalenddiv").show()
	else
		$("#scheduleintervalenddiv").hide()

	$('#scheduleinterval').change ->
		if $(this).is(':checked')
			$("#scheduleintervalenddiv").show()
		else
			$("#scheduleintervalenddiv").hide()

	$('.datetimepicker').datetimepicker locale: 'pl'

	$('#schedules_search input').keyup ->
		$.get($("#schedules_search").attr("action"), $("#schedules_search").serialize(), null, 'script');
		false

$(document).on 'click', '.sortinglink, #schedules .pagination a', (e) ->
	jQuery ($) ->
		$.getScript e.currentTarget.href
	false