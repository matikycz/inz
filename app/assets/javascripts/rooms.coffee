# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:load ready', ->
	$('#rooms_search input').keyup ->
		$.get($("#rooms_search").attr("action"), $("#rooms_search").serialize(), null, 'script');
		false

$(document).on 'click', '.sortinglink, #rooms .pagination a', (e) ->
	jQuery ($) ->
		$.getScript e.currentTarget.href
	false