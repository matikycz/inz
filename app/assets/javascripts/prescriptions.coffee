# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:load ready', ->
	$.get('/medicines/info', {field: 'medicine1', query: $('#prescription_position1_medicine').val()}, null, 'script');
	$.get('/medicines/info', {field: 'medicine2', query: $('#prescription_position2_medicine').val()}, null, 'script');
	$.get('/medicines/info', {field: 'medicine3', query: $('#prescription_position3_medicine').val()}, null, 'script');
	$.get('/medicines/info', {field: 'medicine4', query: $('#prescription_position4_medicine').val()}, null, 'script');
	$.get('/medicines/info', {field: 'medicine5', query: $('#prescription_position5_medicine').val()}, null, 'script');

	$('#prescription_position1_medicine').on "propertychange change keyup paste input",  ->
		$.get('/medicines/info', {field: 'medicine1', query: $('#prescription_position1_medicine').val()}, null, 'script');

	$('#prescription_position2_medicine').on "propertychange change keyup paste input",  ->
		$.get('/medicines/info', {field: 'medicine2', query: $('#prescription_position2_medicine').val()}, null, 'script');

	$('#prescription_position3_medicine').on "propertychange change keyup paste input",  ->
		$.get('/medicines/info', {field: 'medicine3', query: $('#prescription_position3_medicine').val()}, null, 'script');

	$('#prescription_position4_medicine').on "propertychange change keyup paste input",  ->
		$.get('/medicines/info', {field: 'medicine4', query: $('#prescription_position4_medicine').val()}, null, 'script');

	$('#prescription_position5_medicine').on "propertychange change keyup paste input",  ->
		$.get('/medicines/info', {field: 'medicine5', query: $('#prescription_position5_medicine').val()}, null, 'script');


$(document).on 'click', '#medicinelink', (e) ->
	window.open(this.href, 'popUpWindow','resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
	false