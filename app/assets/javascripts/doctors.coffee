# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'page:load ready', ->
	$('#datetimepicker').datetimepicker locale: 'pl', format: 'L'

	$('#doctors_search input').keyup ->
		$.get($("#doctors_search").attr("action"), $("#doctors_search").serialize(), null, 'script');
		false

$(document).on 'click', '.sortinglink, #doctors .pagination a', (e) ->
	jQuery ($) ->
		$.getScript e.currentTarget.href
	false