# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'page:load ready', ->
	# $('#medicalvisitformdoctors').css('max-height', $('#medicalvisitform').height()+'px');
	$('#medicalvisitformdoctorstable').css('max-height', $(window).height()*0.70);

	($('.datetimepickerdatetime').datetimepicker locale: 'pl', stepping: 15).on 'dp.change', ->
		$.get('/medical_visits/doctor_visits', $("#medicalvisitform > form").serialize(), null, 'script');

	# $('#medical_visit_doctor').change ->
	# 	$.get('/medical_visits/doctor_visits', $("#new_medical_visit").serialize(), null, 'script');

	$('#medical_visit_doctor').change ->
		$.get('/medical_visits/doctor_visits', $("#medicalvisitform > form").serialize(), null, 'script');

	($('#datetimesearchpicker').datetimepicker locale: 'pl', format: 'L', defaultDate: new Date(), minDate: new Date()).on 'dp.change', ->
		$.get($("#medical_visits_search_date").attr("action"), $("#medical_visits_search_date").serialize(), null, 'script');
		false

	$('#medical_visit_patient').on "propertychange change keyup paste input",  ->
		$.get('/medical_visits/patient_change', $("#new_medical_visit").serialize(), null, 'script');


$(document).on 'click', '.sortinglink, #visits .pagination a', (e) ->
	jQuery ($) ->
		$.getScript e.currentTarget.href
	false

$(document).on 'focus', '#datetimesearchpicker', (e) ->
	($('#datetimesearchpicker').datetimepicker locale: 'pl', format: 'L', defaultDate: new Date(), minDate: new Date(new Date() - 24*60*60*1000)).on 'dp.change', ->
		$.get($("#medical_visits_search_date").attr("action"), $("#medical_visits_search_date").serialize(), null, 'script');
		false

$(document).on 'click', '.input-group-addon', (e) ->
	($('#datetimesearchpicker').datetimepicker locale: 'pl', format: 'L', defaultDate: new Date(), minDate: new Date(new Date() - 24*60*60*1000)).on 'dp.change', ->
		$.get($("#medical_visits_search_date").attr("action"), $("#medical_visits_search_date").serialize(), null, 'script');
		false