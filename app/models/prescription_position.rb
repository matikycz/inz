class PrescriptionPosition < ActiveRecord::Base
  belongs_to :prescription
  self.inheritance_column = nil
end
