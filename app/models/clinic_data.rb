class ClinicData < ActiveRecord::Base
	@@name = "Przychodnia"
	@@country = "Polska"
	@@city = 27098
	@@street = "Armii Krajowej"
	@@address = "11/2a"
	@@regon = "123-234-456"
	@@phone = "999999999"
	@@fax = "888888888"
	cattr_accessor :name, :country, :city, :street, :address, :regon, :phone, :fax
end
