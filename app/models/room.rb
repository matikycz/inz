class Room < ActiveRecord::Base
    validates :name, presence: true
    validates :number, presence: true, format: {with: /\A[0-9]+\z/, message: "Nieprawidłowy numer gabinetu"},  uniqueness: true

	def self.search(search)
  	if search
  		where("name LIKE :query or number LIKE :query",query: "%#{search}%")
  	else
		all
		end
  end
end
