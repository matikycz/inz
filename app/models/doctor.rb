class Doctor < ActiveRecord::Base

	has_one :user

  validates :degree, :specialization, :employmentDate, presence: true
  validates :doctorRightNumber, presence: true, uniqueness: true, format: {with: /[0-9]{7}/, message: "jest nieprawidłowy"}, length: { is: 7 }

end
