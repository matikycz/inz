class Prescription < ActiveRecord::Base
	belongs_to :doctor, class_name: :User
  belongs_to :patient, class_name: :User
  has_many :prescription_positions, :dependent => :delete_all
  accepts_nested_attributes_for :prescription_positions
  belongs_to :medical_visit

  validates :doctor, :patient, :drawDate, :realizationDate, presence: true
  validates :number, presence: true, uniqueness: true
end
