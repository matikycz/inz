class Examination < ActiveRecord::Base
	belongs_to :nurse, class_name: :User
  belongs_to :patient, class_name: :User
  has_many :examinationpositions, :dependent => :delete_all
	validates :name, :date, :description, presence: true

	def self.search(search)
    if search
      where("name LIKE :query or surname LIKE :query or login LIKE :query or concat(name, ' ', surname) like :query",query: "%#{search}%")
    else
      all
    end
  end
end
