class Admin < ActiveRecord::Base
	has_one :user
	validates :ismain, :inclusion => {:in => [true, false]}, default: false
end