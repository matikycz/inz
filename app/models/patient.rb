class Patient < ActiveRecord::Base

	has_one :user

  belongs_to :nfzbranch, class_name: "NfzBranch"
  belongs_to :maindoctor, class_name: "Doctor"

  validates :insurancenumber, presence: true, uniqueness: true, format: {with: /\A[0-9]{11,13}\z/, message: "ma nieprawidłowy format"}, length: { in: 11..13 }
  validates :registrationdate, :nfzbranch, :maindoctor, presence: true
end
