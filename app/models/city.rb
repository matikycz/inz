class City < ActiveRecord::Base

  has_many :users
  belongs_to :province
  belongs_to :district

  validates :name, :commune, :province, :district, presence: true
  validates :city_code, presence: true, format: {with: /[0-9]{2}-[0-9]{3}/, message: "Nieprawidłowy kod pocztowy"}

  def full_name
  	"#{self.name} (#{self.city_code})"
 	end

 	def to_s
 		"#{self.name} (#{self.city_code})"
 	end
end
