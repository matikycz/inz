class Receptionist < ActiveRecord::Base
	has_one :user

  validates :position, :employmentDate, presence: true
end
