class Examinationposition < ActiveRecord::Base
  belongs_to :examination
  validates :name, :result, :norm, presence: true
end
