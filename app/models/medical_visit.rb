class MedicalVisit < ActiveRecord::Base
  belongs_to :visit_description
  belongs_to :doctor, class_name: :User
  belongs_to :patient, class_name: :User
  has_many :prescriptions

  validates :doctor, :patient, :startTime, :patient_id, presence: true
end
