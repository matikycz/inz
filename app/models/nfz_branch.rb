class NfzBranch < ActiveRecord::Base
	validates :identifier, presence: true, uniqueness: true
	validates :name, presence: true
end
