class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  belongs_to :admin
  accepts_nested_attributes_for :admin

  belongs_to :doctor
  accepts_nested_attributes_for :doctor

  belongs_to :nurse
  accepts_nested_attributes_for :nurse

  belongs_to :receptionist
  accepts_nested_attributes_for :receptionist

  belongs_to :patient
  accepts_nested_attributes_for :patient

  belongs_to :city
  accepts_nested_attributes_for :city

  has_many :schedueles

  has_many :examinations

  has_many :chronic_diseases

  has_many :chronic_medicines

  has_many :alergic_medicines

  validates :login, presence: true, uniqueness: true
  validates :name, :surname, :encrypted_password, :address, :sex, :city, :city_id, presence: true
  validates :pesel, presence: true, uniqueness: true, format: {with: /\A[0-9]{11}\z/, message: "ma nieprawidłowy format"}, length: { is: 11 }
  validates :phone, presence: true, format: {with: /(\A\+[0-9]{2,3})?[0-9]{8,9}\z/, message: "ma nieprawidłowy format"}, length: { in: 8..9 }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:login]

  def admin?
    admin.present?
  end

  def doctor?
    doctor.present?
  end

  def nurse?
    nurse.present?
  end

  def receptionist?
    receptionist.present?
  end

  def patient?
    patient.present?
  end

  def patient_name
    "#{self.name} #{self.surname} (#{self.pesel})"
  end

  def to_s
    "#{self.name} #{self.surname} (#{self.pesel})"
  end

  def self.search(search)
    if search
      where("name LIKE :query or surname LIKE :query or login LIKE :query or concat(name, ' ', surname) like :query",query: "%#{search}%")
    else
      all
    end
  end
end
