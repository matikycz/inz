class VisitDescription < ActiveRecord::Base
	has_one :medicalVisit
	belongs_to :disease
end
