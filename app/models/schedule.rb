class Schedule < ActiveRecord::Base
	belongs_to :doctor, class_name: :User
  belongs_to :room
  validates :startTime, :endTime, :interval, :doctor, :room, presence: true
  validates :interval, inclusion: { in: [0, 1] }

	def self.validate_schedule(schedule)
  	true
  end

  def self.search(search)
  	if search
      all.joins(:doctor).joins(:room).where("users.name LIKE :query or users.surname LIKE :query or rooms.name LIKE :query or concat(users.name, ' ', users.surname) like :query or rooms.number like :query", query: "%#{search}%")
  	else
		all
		end
	end

  def self.doctor_visits(doctor, date)
    schedules = []
    where(doctor: doctor).each { |schedule|
      if schedule.interval == 0
        if schedule.startTime.to_date == date.to_date
          schedules.push schedule
        end
      else
        if (schedule.intervalEnd.nil? or schedule.intervalEnd.to_date >= date.to_date) and schedule.startTime.wday == date.wday
          schedules.push schedule
        end
      end
    }
    schedules
  end

  def self.visit_room(doctor, date)
    room_number = 0
    where(doctor: doctor).each { |schedule|
      if schedule.interval == 0
        if schedule.startTime.to_date == date.to_date
          room_number = schedule.room.number
        end
      else
        if (schedule.intervalEnd.nil? or schedule.intervalEnd.to_date >= date.to_date) and schedule.startTime.wday == date.wday
          room_number = schedule.room.number
        end
      end
    }
    room_number
  end
end
