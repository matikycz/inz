Rails.application.routes.draw do
  root 'home#index'
  get 'home/index'

  get 'admins/index'
  get 'admins/show'
  get 'admins/new'
  post 'admins/create'
  get 'admins/edit'
  put 'admins/update'
  delete 'admins/destroy'

  get 'doctors/index'
  get 'doctors/show'
  get 'doctors/new'
  post 'doctors/create'
  get 'doctors/edit'
  put 'doctors/update'
  delete 'doctors/destroy'

  get 'nurses/index'
  get 'nurses/show'
  get 'nurses/new'
  post 'nurses/create'
  get 'nurses/edit'
  put 'nurses/update'
  delete 'nurses/destroy'

  get 'receptionists/index'
  get 'receptionists/show'
  get 'receptionists/new'
  post 'receptionists/create'
  get 'receptionists/edit'
  put 'receptionists/update'
  delete 'receptionists/destroy'

  get 'patients/index'
  get 'patients/show'
  get 'patients/new'
  post 'patients/create'
  get 'patients/edit'
  put 'patients/update'
  delete 'patients/destroy'

  get 'rooms/index'
  get 'rooms/show'
  get 'rooms/new'
  post 'rooms/create'
  get 'rooms/edit'
  put 'rooms/update'
  delete 'rooms/destroy'

  get 'schedules/index'
  get 'schedules/show'
  get 'schedules/new'
  post 'schedules/create'
  get 'schedules/edit'
  put 'schedules/update'
  delete 'schedules/destroy'

  get 'medical_visits/index'
  get 'medical_visits/show'
  get 'medical_visits/new'
  post 'medical_visits/create'
  get 'medical_visits/edit'
  put 'medical_visits/update'
  delete 'medical_visits/destroy'
  get 'medical_visits/doctor_visits'
  get 'medical_visits/patient_change'

  get 'examinations/index'
  get 'examinations/show'
  get 'examinations/new'
  post 'examinations/create'
  get 'examinations/edit'
  put 'examinations/update'
  delete 'examinations/destroy'

  get 'medicines/show'
  get 'medicines/info'

  #get 'medical_visits/:medical_visit_id/prescriptions/pdf/:id' => 'prescriptions#print', as: 'prescriptions_print'

  resources :medical_visits, only: [] do
    resources :visit_descriptions, :prescriptions
  end
  resources :examinations, only: [] do
    resources :examinationpositions
  end
  resources :users, only: [] do
    resources :chronic_diseases, :chronic_medicines, :alergic_medicines
  end

  devise_for :users, controllers: { sessions: "users/sessions", passwords: "users/passwords",  registrations: "users/registrations" }
  #, skip: [:sessions]
  #as :person do
  #  get "/users/sign_in(.:format)" => "users/sessions#new", as: "new_user_session"
  #end

  devise_scope :user do
    get "/login" => "users/sessions#new"
  end

  resources :admins, only: [:autocomplete_city_name] do
    get :autocomplete_city_name, :on => :collection
  end

  resources :doctors, only: [:autocomplete_city_name] do
    get :autocomplete_city_name, :on => :collection
  end

  resources :nurses, only: [:autocomplete_city_name] do
    get :autocomplete_city_name, :on => :collection
  end

  resources :receptionists, only: [:autocomplete_city_name] do
    get :autocomplete_city_name, :on => :collection
  end

  resources :patients, only: [:autocomplete_city_name] do
    get :autocomplete_city_name, :on => :collection
  end

  resources :medical_visits, only: [:autocomplete_patient_name] do
    get :autocomplete_patient_name, :on => :collection
  end

  resources :examinations, only: [:autocomplete_patient_name] do
    get :autocomplete_patient_name, :on => :collection
  end

  # resources :receptionists do
  #   get :autocomplete_city_name, :on => :collection
  # end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
