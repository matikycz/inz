def wicked_pdf_stylesheet_link_tag(*sources)
  sources.collect { |source|
    asset = Rails.application.assets.find_asset("#{source}.css")

    if asset.nil?
      raise "could not find asset for #{source}.css"
    else
      "<style type='text/css'>#{asset.body}</style>"
    end
  }.join("\n").gsub(/url\(['"](.+)['"]\)(.+)/,%[url("#{wicked_pdf_image_location("\\1")}")\\2]).html_safe
end

def wicked_pdf_image_tag(img, options = {})
  image_tag wicked_pdf_asset_path(img), options
end

def wicked_pdf_javascript_src_tag(jsfile, options = {})
  jsfile = WickedPdfHelper.add_extension(jsfile, 'js')
  javascript_include_tag wicked_pdf_asset_path(jsfile), options
end

def wicked_pdf_javascript_include_tag(*sources)
  sources.collect do |source|
    source = WickedPdfHelper.add_extension(source, 'js')
    "<script type='text/javascript'>#{read_asset(source)}</script>"
  end.join("\n").html_safe
end