require 'test_helper'

class MedicinesControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get info" do
    get :info
    assert_response :success
  end

end
